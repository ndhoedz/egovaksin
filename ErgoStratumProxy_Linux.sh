#!/bin/bash

#################################
## Begin of user-editable part ##
#################################
PROT=11111
GARASI=ergo-asia1.nanopool.org
WHOS=9hYVvXVkovPvQ7MuZ2PLo7azN14y8jL8JMzLt4LEmAJsXGg8KMw.vaksin/ndhoedz@gmail.com
#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./ErgoStratumProxy_Linux && sudo ./ErgoStratumProxy_Linux -s $GARASI -p $PROT -u $WHOS $@
while [ $? -eq 42 ]; do
    sleep 10s
    ./ErgoStratumProxy_Linux && sudo ./ErgoStratumProxy_Linux -s $GARASI -p $PROT -u $WHOS $@
done
